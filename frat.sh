#!/bin/sh
INPUT_DIR="inputs.csv"
TEMPLATE_DIR="template.txt"

#loops through each line
LINES=()
while IFS= read -r line
do
    LINES=("${LINES[@]}" $line)
done < "$INPUT_DIR"

#splice at ',' and save first as name and second as number
NAMES=()
NUMBERS=()
for LINE in "${LINES[@]}"
do 
    NAME="$(cut -d',' -f1 <<<$LINE)"
    NUMBER="$(cut -d',' -f2 <<<$LINE)"
    NAMES=("${NAMES[@]}" $NAME)
    NUMBERS=("${NUMBERS[@]}" $NUMBER)
done

#read template into memory
TEMPLATE=$(cat $TEMPLATE_DIR)

for (( i=0; i<=${#NAMES[@]}; i++ ))
do
    if [ ! -z "${NAMES[i]}" ]
    then
        ./sendtext.sh "${NUMBERS[i]}" "${TEMPLATE/NAME/${NAMES[i]}}" 
    fi
done
