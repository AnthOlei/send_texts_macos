This is a program for sending a template text to many people at once. The people do not have to be in your contacts (This is
due to a loophole in apple security, meaning it probably will be patched soon. Apple only allows apple scripts to send
texts to existing contacts) Original usecase is for my roomate who is in a Fraternity who needs to send out a lot of recruitment texts
that follow the same exact format. He asked me to write this program, so I did - it was fun to learn some bash.


inputs.csv MUST contain a csv formatted like this:

name,phone
name,phone
name,phone

and there MUST be an empty line at the end, otherwise the program will miss the last name.

you must be on a macbook for this to work.

the system will ask for permissions, accept all the permissions that it asks for.

run "sudo chmod 777 sendtext.txt && sudo chmod 777 testing.sh" or else your system will be pissed.
it will ask for your password to your computer, this is because you (again) are changing permissions.

fillout template.txt with the text template. NAME will be replaced with the FIRST column (name) of the CSV.

finally, to run the script run "./frat.sh". THIS CANNOT BE REVERSED. The texts sent are permanent, and 
cannot be reversed. TRIPLE CHECK that your template is correct. It's advisable to first test the template out on a couple
of your friends. the recepient need not really exist, so inputting a fake (too long works best) phone number will still send
the template to the phone number. 